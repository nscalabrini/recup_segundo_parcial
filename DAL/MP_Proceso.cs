﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Proceso
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Proceso proceso)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nom", proceso.Nombre));
            parameters.Add(acceso.CrearParametro("@tiempo", proceso.Tiempo));
            acceso.Escribir("PROCESO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Proceso proceso)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nom", proceso.Nombre));
            parameters.Add(acceso.CrearParametro("@tiempo", proceso.Tiempo));
            parameters.Add(acceso.CrearParametro("@id", proceso.Id));
            acceso.Escribir("PROCESO_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Proceso proceso)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", proceso.Id));
            acceso.Escribir("PROCESO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Proceso> Listar()
        {
            List<BE.Proceso> lista = new List<BE.Proceso>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PROCESO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Proceso p = new BE.Proceso();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                p.Tiempo = registro[2].ToString();
                lista.Add(p);
            }
            return lista;
        }


    }


}
