﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Proceso
    {

        DAL.MP_Proceso map = new DAL.MP_Proceso();

        public void Grabar(BE.Proceso proceso)
        {
            if (proceso.Id <= 0)
            {

                map.Insertar(proceso);
            }
           else
            {
                map.Editar(proceso);
            }




        }
        public void Borrar(BE.Proceso proceso)
        {
            map.Borrar(proceso);
        }

        public List<BE.Proceso> Listar()
        {
            return map.Listar();

        }


    }
}
