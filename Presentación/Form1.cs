﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Presentación
{
    
    public partial class Form1 : Form
    {
        DataSet ds = new DataSet();
        BLL.Proceso gestor = new BLL.Proceso();
        public Form1()
        {
            InitializeComponent();
        }

        private void enlazar()
        {
            listBox1.DataSource = null;

            listBox1.DataSource = Process.GetProcesses();

            listBox1.DisplayMember = "ProcessName";

        }

        private void Enlazar2()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();

        }




        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {

                Process p = (Process)listBox1.SelectedItem;

                label1.Text = p.ProcessName;

                label2.Text = "Esta funcionando bien? " + p.Responding.ToString();

                try
                {
                    label3.Text = p.StartTime.ToString();
                }
                catch
                {
                    label3.Text = "No tiene permisos";
                }
                try
                {
                    label4.Text = p.TotalProcessorTime.ToString();
                }
                catch
                {
                    label4.Text = "No tiene permisos";
                }



            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            enlazar();
            Enlazar2();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                Process.Start(textBox1.Text);
            }
            else
            {
                Process.Start(textBox1.Text, textBox2.Text);
            }

            enlazar();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process p= (Process)listBox1.SelectedItem;

        
            try
            {

                BE.Proceso pro = new BE.Proceso();
                label7.Text = p.ProcessName;
                label8.Text = p.TotalProcessorTime.ToString();
                pro.Nombre = label7.Text;
                pro.Tiempo = label8.Text;
                pro.Id = int.Parse(textBox3.Text);
                gestor.Grabar(pro);
                Enlazar2();


            }
            catch
            {
                label7.Text = "No tiene Permiso de Grabar";

            }


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            label7.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            label8.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            
        }

        private void Abrir_Click(object sender, EventArgs e)
        {

            OFD.InitialDirectory = "C:\\Users\norberto.scalabrini\\Desktop\\TMP\\UAI\\LUG Local\\LUG2020\\Segundo Parcial\\Segundo Parcial Norberto_Scalabrini\\Recuperatorio";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                string esquema = OFD.FileName.Replace(".xml", "_esquema.xml");
                if (File.Exists(esquema))
                {
                    ds.ReadXmlSchema(esquema);
                }


                ds.ReadXml(OFD.FileName);

            }
            enlazar3();

        }

        void enlazar3()
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = ds.Tables[0];

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process p = (Process)listBox1.SelectedItem;

            try
            {
                label10.Text = p.ProcessName;
                label11.Text = p.TotalProcessorTime.ToString();
                DataRow registro = ds.Tables[0].NewRow();
                registro[0] = label10.Text;
                registro[1] = label11.Text;
                ds.Tables[0].Rows.Add(registro);
                ds.WriteXml(OFD.FileName);
                enlazar3();
            }
            catch
            {
                label10.Text = "No tiene Permiso sobre el Proceso";            
            
            }

            
        }

        private void nuevoXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (SFD.ShowDialog() == DialogResult.OK)
            {
                ds.Tables.Clear();

                ds.Tables.Add(new DataTable("PROCESO"));

                ds.Tables["PROCESO"].Columns.Add(new DataColumn("nombre"));

                ds.Tables["PROCESO"].Columns.Add(new DataColumn("tiempo"));

                string esquema = SFD.FileName.Replace(".xml", "_esquema.xml");

                ds.WriteXmlSchema(esquema);
                ds.WriteXml(SFD.FileName);
            }



        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void Borrar_Click(object sender, EventArgs e)
        {

            if (indice > -1)
            {
                ds.Tables[0].Rows.RemoveAt(indice);
                indice = -1;
                ds.WriteXml(OFD.FileName);
                enlazar();
            }
        }
        int indice = -1;

        private void button4_Click(object sender, EventArgs e)
        {
            BE.Proceso p = new BE.Proceso();

            p.Id = int.Parse(textBox3.Text);

            gestor.Borrar(p);
            Enlazar2();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
    }
}
